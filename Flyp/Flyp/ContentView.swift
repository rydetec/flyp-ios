//
//  ContentView.swift
//  Flyp
//
//  Created by Ryan Dean on 7/16/20.
//  Copyright © 2020 RydeSoft. All rights reserved.
//

import SwiftUI


var activeDeck: Deck?
var activeCard: Card?
var frontActive: Bool?

struct Card: Identifiable {
	var id = UUID()
	var front: String
	var back: String
	
	mutating func update(_ card: Card) {
		self.front = card.front
		self.back = card.back
	}
}

struct Deck: Identifiable {
	var id = UUID()
	var title: String
	var cards: [Card] = [
	]
	
	mutating func update(_ card: Card) {
		var stack: [Card] = []
		for var c in cards {
			if (c.id == card.id) {
				c.update(card)
			}
			stack.append(c)
		}
		cards = stack
	}
}

var decks: [Deck] = [
	Deck(title: "Welcome to Flyp!", cards: [
		Card(front: "Tap to Flip.", back: "Press ▶️ to go to the next card."),
		Card(front: "How do you edit?", back: "Tap and hold."),
		Card(front: "How do you browse?", back: "▶️ goes to the next card, and ◀️ goes to the previous."),
		Card(front: "How do you study?", back: "Press 🔀 to shuffle the deck, press ✅ to hide a card from the current session.")
	])
]

enum ActiveSheet {
   case first, second
}

struct ContentView: View {
	@State private var isShowingDeck = false
	@State private var activeSheet: ActiveSheet = .first
	
	@State private var isShowingCard = false
	@State private var isFlipped = false
	
	@State private var cardText: String = ""
	
	@State private var isShowingEdit = false

    var body: some View {
    	ZStack {
    		VStack {
    			Text("Decks")
					.font(.title)
					.fontWeight(.bold)
				
    	
				List(decks) { decks in
					Text(decks.title).onTapGesture {
						activeDeck = decks
						self.activeSheet = .first
						self.isShowingDeck.toggle()
					}
				}
			
				Text("New Deck")
				.frame(minWidth: 0, maxWidth: .infinity)
				.padding()
				.background(Color.blue)
				.foregroundColor(.white)
				.cornerRadius(15)
				.padding(5)
				.onTapGesture {
						decks.append(Deck(title: "Deck " + decks.count.description))
						activeDeck = decks.last
						self.activeSheet = .first
						self.isShowingDeck.toggle()
					}
			}
		}
		.sheet(isPresented: $isShowingDeck) {
			if self.activeSheet == .first {
				ZStack {
					VStack {
						Text(activeDeck!.title)
						.font(.title)
						.fontWeight(.bold)
						
						List(activeDeck!.cards) { cards in
							Text(cards.front).onTapGesture {
								activeCard = cards
								self.isShowingCard.toggle()
							}
						}
						
						Text("New Card")
						.frame(minWidth: 0, maxWidth: .infinity)
						.padding()
						.background(Color.blue)
						.foregroundColor(.white)
						.cornerRadius(15)
						.padding(5)
						.onTapGesture {
							activeDeck!.cards.append(Card(front: "Card " + activeDeck!.cards.count.description, back: "Back"))
							activeCard = activeDeck!.cards.last
							self.isShowingCard.toggle()
						}
					}
				}
				.sheet(isPresented: self.$isShowingCard) {
					VStack {
						Text(activeDeck!.title)
						
						Text(self.cardText)
						.frame(minWidth: 0, idealWidth: 180, maxWidth: .infinity, minHeight: 0, idealHeight: 160, maxHeight: 160, alignment: .center)
						.padding()
						.background(Color.gray)
						.foregroundColor(.black)
						.cornerRadius(15)
						.padding(5)
						.onTapGesture {
							
							self.isFlipped.toggle()
							
							if (self.isFlipped) {
								self.cardText = activeCard!.back
							} else {
								self.cardText = activeCard!.front
							}
						}
						.onAppear {
							self.cardText = activeCard!.front
							self.isFlipped = false
						}
						.onLongPressGesture {
							if (self.isFlipped) {
								frontActive = false
							} else {
								frontActive = true
							}
						
							self.isShowingEdit.toggle()
						}
						.textFieldAlert(isShowing: self.$isShowingEdit, text: self.$cardText, title: "Edit")
						
						
						HStack {
							Text("◀️")
							.onTapGesture {
								
								activeCard = activeDeck!.cards.popLast()
								activeDeck!.cards.insert(activeCard!, at: 0)
								
								self.cardText = activeCard!.front
								self.isFlipped = false
							}
							
							Text("✅")
							.onTapGesture {
								
								activeDeck!.cards.remove(at: 0)
								
								if (activeDeck!.cards.isEmpty) {
									self.isShowingCard = false
									activeDeck = decks.filter{
											$0.id == activeDeck!.id
										}
										.first
								} else {
									activeCard = activeDeck!.cards[0]
								
									self.cardText = activeCard!.front
									self.isFlipped = false
								}
							}
							
							Text("🔀")
							.onTapGesture {
								activeDeck!.cards.remove(at: 0)
								activeDeck!.cards.append(activeCard!)
								
								activeDeck!.cards.shuffle()
								
								activeCard = activeDeck!.cards[0]
								
								self.cardText = activeCard!.front
								self.isFlipped = false
							}
							
							Text("▶️")
							.onTapGesture {
								
								activeDeck!.cards.remove(at: 0)
								activeDeck!.cards.append(activeCard!)
								activeCard = activeDeck!.cards[0]
								
								self.cardText = activeCard!.front
								self.isFlipped = false
							}
						}
						.font(.title)
						.frame(minWidth: 0, idealWidth: 180, maxWidth: .infinity, minHeight: 0, idealHeight: 160, maxHeight: 160, alignment: .top)
						.padding()
					}
				}
			} else {
				VStack {
					Text("New")
				}
			}
			
		}
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


struct TextFieldAlert<Presenting>: View where Presenting: View {

    @Binding var isShowing: Bool
    @Binding var text: String
    let presenting: Presenting
    let title: String

    var body: some View {
        GeometryReader { (deviceSize: GeometryProxy) in
            ZStack {
                self.presenting
					.disabled(self.isShowing)
                VStack {
                    Text(self.title)
					TextField("Test", text: self.$text, onEditingChanged: {_ in }, onCommit: {
					
							if (frontActive!) {
                        		activeCard!.front = self.text
							} else {
								activeCard!.back = self.text
							}
							
							var c = activeDeck!.cards.filter( {$0.id == activeCard!.id}).first!
							c.front = activeCard!.front
							c.back = activeCard!.back
							
							
							var stack: [Deck] = []
							for var d in decks {
								if (d.id == activeDeck!.id) {
									d.update(c)
									activeDeck = d
								}
								stack.append(d)
							}
							decks = stack
							
							withAnimation {
                                self.isShowing.toggle()
                            }
					})
					.multilineTextAlignment(.center)
                	.lineLimit(1)
                	.id(self.isShowing)
                
                    Divider()
                    HStack {
                        Button(action: {
                            withAnimation {
                                self.isShowing.toggle()
                            }
                        }) {
                            Text("Dismiss")
                        }
                    }
                }
                .padding()
                .background(Color.white)
                .frame(
                    width: deviceSize.size.width*0.7,
                    height: deviceSize.size.height*0.7
                )
                .shadow(radius: 1)
                .opacity(self.isShowing ? 1 : 0)
            }
        }
    }

}

extension View {

    func textFieldAlert(isShowing: Binding<Bool>,
                        text: Binding<String>,
                        title: String) -> some View {
        TextFieldAlert(isShowing: isShowing,
                       text: text,
                       presenting: self,
                       title: title)
    }

}
